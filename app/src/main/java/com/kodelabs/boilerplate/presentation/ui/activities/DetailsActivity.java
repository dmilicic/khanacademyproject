package com.kodelabs.boilerplate.presentation.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kodelabs.boilerplate.R;
import com.kodelabs.boilerplate.presentation.ui.BaseView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity implements BaseView {

    public static final String KEY_IMAGE_URL            = "key_image_url";
    public static final String KEY_EXTENDED_DESCRIPTION = "key_extended_description";

    @Bind(R.id.badge_icon)
    ImageView mImageView;

    @Bind(R.id.badge_extended_description)
    TextView mDescription;

    @Bind(R.id.progress_bar)
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        init();
    }

    private void init() {

        showProgress();

        // let's extract the data
        String imageUrl = getIntent().getStringExtra(KEY_IMAGE_URL);
        String extendedDescription = getIntent().getStringExtra(KEY_EXTENDED_DESCRIPTION);

        // load the image with a progress animation
        Picasso.with(this)
                .load(imageUrl)
                .into(mImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        hideProgress();
                    }

                    @Override
                    public void onError() {
                        hideProgress();
                        Toast.makeText(DetailsActivity.this, "Couldn't load the image!", Toast.LENGTH_LONG).show();
                    }
                });

        mDescription.setText(extendedDescription);
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {

    }
}
