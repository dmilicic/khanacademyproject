package com.kodelabs.boilerplate.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.kodelabs.boilerplate.R;
import com.kodelabs.boilerplate.domain.model.Badge;
import com.kodelabs.boilerplate.domain.model.BadgeIcons;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by dmilicic on 3/14/16.
 */
public class BadgeAdapter extends BaseAdapter {

    private List<Badge> mBadgeList;
    private Context     mContext;


    public BadgeAdapter(List<Badge> badgeList, Context context) {
        mBadgeList = badgeList;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mBadgeList.size();
    }

    @Override
    public Object getItem(int position) {
        return mBadgeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View badgeView = convertView;

        if (convertView == null) {

            // inflate the badge view
            badgeView = LayoutInflater.from(mContext).inflate(R.layout.badge_view, null);

            badgeView = setupView(badgeView, position);
        } else {
            badgeView = setupView(badgeView, position);
        }

        return badgeView;
    }

    private View setupView(View badgeView, int position) {

        // get a badge for this position
        Badge badge = mBadgeList.get(position);

        // setup the icon with picasso
        ImageView imageView = (ImageView) badgeView.findViewById(R.id.badge_icon);

        BadgeIcons icons = badge.getBadgeIcons();
        String iconSrc = badge.getIconSrc();

        // we have a better quality picture
        if (icons != null) {
            iconSrc = icons.getIconEmail();
        }

        // load the image
        Picasso.with(mContext).load(iconSrc).into(imageView);

        // insert the short description
        TextView descriptionView = (TextView) badgeView.findViewById(R.id.badge_description);
        descriptionView.setText(badge.getDescription());

        badgeView.setLayoutParams(new GridView.LayoutParams(400, 350));

        return badgeView;
    }
}
