package com.kodelabs.boilerplate.presentation.presenters.impl;

import com.kodelabs.boilerplate.domain.executor.Executor;
import com.kodelabs.boilerplate.domain.executor.MainThread;
import com.kodelabs.boilerplate.domain.interactors.RetrieveBadgesInteractor;
import com.kodelabs.boilerplate.domain.interactors.impl.RetrieveBadgesInteractorImpl;
import com.kodelabs.boilerplate.domain.model.Badge;
import com.kodelabs.boilerplate.domain.repository.BadgeRepository;
import com.kodelabs.boilerplate.presentation.presenters.MainPresenter;
import com.kodelabs.boilerplate.presentation.presenters.base.AbstractPresenter;

import java.util.List;

/**
 * Our main presenter will be a singleton to avoid restarting expensive badge retrieval each time a new Activity is created.
 * <p/>
 */
public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
        RetrieveBadgesInteractor.Callback {

    private static MainPresenter sMainPresenter;

    private MainPresenter.View mView;
    private BadgeRepository    mBadgeRepository;

    // this will store our retrieved badges
    private List<Badge> mBadgeList;

    /**
     * Gets an instance of the main presenter.
     */
    public static MainPresenter getInstance(Executor executor, MainThread mainThread,
                                            View view, BadgeRepository badgeRepository) {

        if (sMainPresenter == null) {
            sMainPresenter = new MainPresenterImpl(executor, mainThread, view, badgeRepository);
        }

        // refresh a reference to our new activity
        sMainPresenter.setView(view);

        return sMainPresenter;
    }

    /* A private constructor for this singleton */
    private MainPresenterImpl(Executor executor, MainThread mainThread,
                              View view, BadgeRepository badgeRepository) {
        super(executor, mainThread);
        mView = view;
        mBadgeRepository = badgeRepository;
    }

    public void setView(MainPresenter.View view) {
        this.mView = view;
    }

    @Override
    public void resume() {

        // in case we haven't yet retrieved the badges, let's do it now
        if (mBadgeList == null) {

            // show a progress bar while badges are loading
            mView.showProgress();

            // let's instantiate and run our interactor
            RetrieveBadgesInteractor interactor = new RetrieveBadgesInteractorImpl(
                    mExecutor,
                    mMainThread,
                    this,  // our callback
                    mBadgeRepository
            );

            interactor.execute();
        } else {
            // we already have badges in memory
            onBadgesRetrieved(mBadgeList);
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onBadgesRetrieved(List<Badge> badges) {
        mBadgeList = badges;

        // display the badges to the user if the UI exists
        if (mView != null) {

            // we have received the badges, hide the progress bar
            mView.hideProgress();

            mView.showBadges(badges);
        }
    }
}
