package com.kodelabs.boilerplate.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;

import com.kodelabs.boilerplate.R;
import com.kodelabs.boilerplate.domain.executor.impl.ThreadExecutor;
import com.kodelabs.boilerplate.domain.model.Badge;
import com.kodelabs.boilerplate.network.BadgeRepositoryImpl;
import com.kodelabs.boilerplate.presentation.presenters.MainPresenter;
import com.kodelabs.boilerplate.presentation.presenters.MainPresenter.View;
import com.kodelabs.boilerplate.presentation.presenters.impl.MainPresenterImpl;
import com.kodelabs.boilerplate.presentation.ui.adapters.BadgeAdapter;
import com.kodelabs.boilerplate.threading.MainThreadImpl;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View {

    @Bind(R.id.gridview)
    GridView mBadgeGrid;

    @Bind(R.id.progress_bar)
    ProgressBar mProgressBar;

    private MainPresenter mMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // hide the progress bar when first starting an activity
        hideProgress();

        // setup our click listener for each badge
        mBadgeGrid.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, android.view.View view, int position, long id) {

                // let's retrieve this badge info
                ListAdapter adapter = mBadgeGrid.getAdapter();
                Badge badge = (Badge) adapter.getItem(position);

                // create a new intent and send the required data
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                intent.putExtra(DetailsActivity.KEY_IMAGE_URL, badge.getBadgeIcons().getIconLarge());
                intent.putExtra(DetailsActivity.KEY_EXTENDED_DESCRIPTION, badge.getExtendedDescription());

                startActivity(intent);
            }
        });

        // let's instantiate our presenter
        mMainPresenter = MainPresenterImpl.getInstance(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                new BadgeRepositoryImpl()
        );
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Badge retrieval starts when this activity resumes
        mMainPresenter.resume();
    }

    @Override
    public void showBadges(List<Badge> badges) {
        BadgeAdapter adapter = new BadgeAdapter(badges, this);
        mBadgeGrid.setAdapter(adapter);
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(android.view.View.VISIBLE);
        mProgressBar.setIndeterminate(true);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(android.view.View.GONE);
    }

    @Override
    public void showError(String message) {

    }
}
