package com.kodelabs.boilerplate.presentation.presenters;

import com.kodelabs.boilerplate.domain.model.Badge;
import com.kodelabs.boilerplate.presentation.presenters.base.BasePresenter;
import com.kodelabs.boilerplate.presentation.ui.BaseView;

import java.util.List;


public interface MainPresenter extends BasePresenter {

    interface View extends BaseView {
        void showBadges(List<Badge> badges);
    }

    void setView(View view);
}
