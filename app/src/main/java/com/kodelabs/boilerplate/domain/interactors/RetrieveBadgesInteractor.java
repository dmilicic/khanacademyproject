package com.kodelabs.boilerplate.domain.interactors;


import com.kodelabs.boilerplate.domain.interactors.base.Interactor;
import com.kodelabs.boilerplate.domain.model.Badge;

import java.util.List;


public interface RetrieveBadgesInteractor extends Interactor {

    interface Callback {

        void onBadgesRetrieved(List<Badge> badges);
    }
}
