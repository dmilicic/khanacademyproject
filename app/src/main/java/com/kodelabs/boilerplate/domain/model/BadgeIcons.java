package com.kodelabs.boilerplate.domain.model;

/**
 * Created by dmilicic on 3/14/16.
 */
public class BadgeIcons {

    private String mIconSmall;

    private String mIconCompact;

    private String mIconLarge;

    private String mIconEmail;

    public BadgeIcons(String iconSmall, String iconCompact, String iconLarge, String iconEmail) {
        mIconSmall = iconSmall;
        mIconCompact = iconCompact;
        mIconLarge = iconLarge;
        mIconEmail = iconEmail;
    }

    public String getIconSmall() {
        return mIconSmall;
    }

    public String getIconCompact() {
        return mIconCompact;
    }

    public String getIconLarge() {
        return mIconLarge;
    }

    public String getIconEmail() {
        return mIconEmail;
    }
}
