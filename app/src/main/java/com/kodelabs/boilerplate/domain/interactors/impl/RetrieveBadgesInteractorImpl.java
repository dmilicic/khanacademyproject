package com.kodelabs.boilerplate.domain.interactors.impl;

import com.kodelabs.boilerplate.domain.executor.Executor;
import com.kodelabs.boilerplate.domain.executor.MainThread;
import com.kodelabs.boilerplate.domain.interactors.RetrieveBadgesInteractor;
import com.kodelabs.boilerplate.domain.interactors.base.AbstractInteractor;
import com.kodelabs.boilerplate.domain.model.Badge;
import com.kodelabs.boilerplate.domain.model.BadgeCategory;
import com.kodelabs.boilerplate.domain.repository.BadgeRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * This interactor retrieves the badges and filters Challenge Patch badges from the rest.
 * <p/>
 */
public class RetrieveBadgesInteractorImpl extends AbstractInteractor implements RetrieveBadgesInteractor {

    private RetrieveBadgesInteractor.Callback mCallback;
    private BadgeRepository                   mBadgeRepository;

    public RetrieveBadgesInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback, BadgeRepository badgeRepository) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mBadgeRepository = badgeRepository;
    }

    @Override
    public void run() {

        // retrieve the badges from our repository
        List<Badge> badges = mBadgeRepository.getAllBadges();

        // let's separate the challenge patch badges to a different array
        final List<Badge> challengePatches = new ArrayList<>();
        for (Badge badge : badges) {

            // we are only interested in challenge patches
            if (badge.getBadgeCategory() == BadgeCategory.CHALLENGE_PATCH) {
                challengePatches.add(badge);
            }
        }

        // let's send our retrieved badges to the UI
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBadgesRetrieved(challengePatches);
            }
        });

    }
}
