package com.kodelabs.boilerplate.domain.repository;

import com.kodelabs.boilerplate.domain.model.Badge;

import java.util.List;

/**
 * This will represent our badge repository which will be retrieved either from the Web or the database.
 */
public interface BadgeRepository {

    List<Badge> getAllBadges();
}
