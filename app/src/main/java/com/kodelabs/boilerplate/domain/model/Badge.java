package com.kodelabs.boilerplate.domain.model;

/**
 * Created by dmilicic on 3/14/16.
 */
public class Badge {

    private String mIconSrc;
    private int    mBadgeCategory;
    private String mDescription;
    private String mName;
    private int    mPoints;
    private String mExtendedDescription;

    private BadgeIcons mBadgeIcons;

    public Badge(String iconSrc, int badgeCategory, String description, String name, int points,
                 String extendedDescription, BadgeIcons badgeIcons) {
        mIconSrc = iconSrc;
        mBadgeCategory = badgeCategory;
        mDescription = description;
        mName = name;
        mPoints = points;
        mExtendedDescription = extendedDescription;
        mBadgeIcons = badgeIcons;
    }

    public BadgeIcons getBadgeIcons() {
        return mBadgeIcons;
    }

    public String getIconSrc() {
        return mIconSrc;
    }

    public int getBadgeCategory() {
        return mBadgeCategory;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getName() {
        return mName;
    }

    public int getPoints() {
        return mPoints;
    }

    public String getExtendedDescription() {
        return mExtendedDescription;
    }
}
