package com.kodelabs.boilerplate.domain.model;

/**
 * Created by dmilicic on 3/14/16.
 */
public class BadgeCategory {
    public static final int METEORITE_BADGE  = 0;
    public static final int MOON_BADGE       = 1;
    public static final int EARTH_BADGE      = 2;
    public static final int SUN_BADGE        = 3;
    public static final int BLACK_HOLE_BADGE = 4;
    public static final int CHALLENGE_PATCH  = 5;
}
