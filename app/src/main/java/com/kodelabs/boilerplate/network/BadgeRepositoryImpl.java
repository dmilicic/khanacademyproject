package com.kodelabs.boilerplate.network;

import com.kodelabs.boilerplate.domain.model.Badge;
import com.kodelabs.boilerplate.domain.repository.BadgeRepository;
import com.kodelabs.boilerplate.network.converters.BadgesConverter;
import com.kodelabs.boilerplate.network.model.BadgeREST;
import com.kodelabs.boilerplate.network.services.BadgeService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmilicic on 3/14/16.
 */
public class BadgeRepositoryImpl implements BadgeRepository {

    @Override
    public List<Badge> getAllBadges() {

        // get the network service responsible for badges
        BadgeService badgeService = RestClient.getService(BadgeService.class);

        // we will return an empty array if we don't retrieve any badges from the server
        List<Badge> result = new ArrayList<>();

        // attempt to download badges over the network
        try {

            // execute the request and extract the body
            List<BadgeREST> badgeRESTList = badgeService.getBadges()
                    .execute()
                    .body();

            // we need to convert the retrieved badges to our domain model before processing
            result = BadgesConverter.convertListToDomainModel(badgeRESTList);

        } catch (IOException | RuntimeException e) { // some network error occured
            e.printStackTrace();
        }

        return result;
    }
}
