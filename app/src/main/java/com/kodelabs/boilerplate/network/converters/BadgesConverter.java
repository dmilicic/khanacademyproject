package com.kodelabs.boilerplate.network.converters;

import com.kodelabs.boilerplate.domain.model.Badge;
import com.kodelabs.boilerplate.domain.model.BadgeIcons;
import com.kodelabs.boilerplate.network.model.BadgeIconsREST;
import com.kodelabs.boilerplate.network.model.BadgeREST;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to convert network models to domain models. This conversion is necessary as network models have
 * additional overhead from our GSON converter.
 * <p/>
 * Created by dmilicic on 3/14/16.
 */
public class BadgesConverter {

    /**
     * Converts a list of network model badges to our domain model badges we can process them.
     */
    public static List<Badge> convertListToDomainModel(List<BadgeREST> RESTbadges) {
        List<Badge> result = new ArrayList<>();

        // convert each network badge to our domain badge model
        for (BadgeREST badge : RESTbadges) {
            Badge convertedBadge = convertToDomainModel(badge);
            result.add(convertedBadge);
        }

        return result;
    }

    /**
     * Converts a single network model badge to a domain model badge.
     */
    public static Badge convertToDomainModel(BadgeREST badge) {
        BadgeIconsREST icons = badge.getIcons();

        // if these badge icons are not set we will use the default icon
        BadgeIcons badgeIcons = null;

        // let's be a bit careful here :)
        if (icons != null) {
            badgeIcons = new BadgeIcons(
                    icons.getIconSmall(),
                    icons.getIconCompact(),
                    icons.getIconLarge(),
                    icons.getIconEmail()
            );
        }

        // extract badge data
        String iconSrc = badge.getIconSrc();
        int category = badge.getBadgeCategory();
        String description = badge.getDescription();
        String extendedDescription = badge.getExtendedDescription();
        String name = badge.getName();
        int points = badge.getPoints();

        return new Badge(iconSrc, category, description, name, points, extendedDescription, badgeIcons);
    }
}
