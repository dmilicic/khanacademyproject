package com.kodelabs.boilerplate.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dmilicic on 3/14/16.
 */
public class BadgeIconsREST {

    @SerializedName("small")
    private String mIconSmall;

    @SerializedName("compact")
    private String mIconCompact;

    @SerializedName("large")
    private String mIconLarge;

    @SerializedName("email")
    private String mIconEmail;

    public BadgeIconsREST(String iconSmall, String iconCompact, String iconLarge, String iconEmail) {
        mIconSmall = iconSmall;
        mIconCompact = iconCompact;
        mIconLarge = iconLarge;
        mIconEmail = iconEmail;
    }

    public String getIconSmall() {
        return mIconSmall;
    }

    public String getIconCompact() {
        return mIconCompact;
    }

    public String getIconLarge() {
        return mIconLarge;
    }

    public String getIconEmail() {
        return mIconEmail;
    }
}
