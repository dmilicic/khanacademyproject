package com.kodelabs.boilerplate.network.services;

import com.kodelabs.boilerplate.network.model.BadgeREST;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * A REST interface describing how badges will be retrieved.
 */
public interface BadgeService {
    /**
     * This endpoint will be used to retrieve all the badges.
     */
    @GET("/api/v1/badges")
    Call<List<BadgeREST>> getBadges();
}
