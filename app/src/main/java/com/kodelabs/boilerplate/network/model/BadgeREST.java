package com.kodelabs.boilerplate.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by dmilicic on 3/14/16.
 */
public class BadgeREST {

    @SerializedName("icon_src")
    private String mIconSrc;

    @SerializedName("badge_category")
    private int mBadgeCategory;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("name")
    private String mName;

    @SerializedName("points")
    private int mPoints;

    @SerializedName("safe_extended_description")
    private String mExtendedDescription;

    @SerializedName("icons")
    private BadgeIconsREST mIcons;

    public BadgeREST(String iconSrc, int badgeCategory, String description, String name, int points,
                     String extendedDescription, BadgeIconsREST icons) {
        mIconSrc = iconSrc;
        mBadgeCategory = badgeCategory;
        mDescription = description;
        mName = name;
        mPoints = points;
        mExtendedDescription = extendedDescription;
        mIcons = icons;
    }

    public BadgeIconsREST getIcons() {
        return mIcons;
    }

    public String getIconSrc() {
        return mIconSrc;
    }

    public int getBadgeCategory() {
        return mBadgeCategory;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getName() {
        return mName;
    }

    public int getPoints() {
        return mPoints;
    }

    public String getExtendedDescription() {
        return mExtendedDescription;
    }
}
